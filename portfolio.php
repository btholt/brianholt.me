<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>portfolio | brianholt.me</title>
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/portfolio.css">
	<link rel="icon" type="image/png" href="favicon.png">
</head>
<body>
<?php include "nav.php" ?>

<div class="module">
	<h2>portfolio</h2>
	<p>
	Coming soon! Real soon! Until then, my <a href="http://linkedin.com/in/btholt" target="_blank">LinkedIn</a> is always up to date.
	</p>
	<p>
		However, in the meantime, feel free to check out some of my experiments.
	</p>
	<ul>
		<li><a href="index3d.php">3D Index</a></li>
		<li><a href="experiments/indexCloth.php">Cloth Index</a></li>
	</ul>
</div>


	
<script type="text/javascript" src="js/global-ck.js"></script>
<?php include_once("analytics.php"); ?>
</body>
</html>