<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>home | brianholt.me</title>
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/home.css">
	<link rel="icon" type="image/png" href="favicon.png">
</head>
<body>
	<section class="siteHead">
		<h1>brian holt</h1>
		<span class="siteHeadText">web developer, innovator, husband, badass</span>
		<div id="uneven_border"></div>
	</section>
	<section class="buttonWrap">
		<div class="buttonBody">
			<div class="col col-1-3">
				<a href="http://blog.brianholt.me"><div id="blog" class="button">blog</div></a>
			</div>
			<div class="col col-1-3">
				<a href="portfolio.php"><div id="portfolio" class="button">portfolio</div></a>
			</div>
			<div class="col col-1-3">
				<a href="workwithme.php"><div id="work" class="button">work with me</div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://twitter.com/holtbt" target="_blank"><div id="twitter" class="button"><img src="../images/twitter-bird-dark-bgs.png"></div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://gplus.to/btholt" target="_blank"><div id="gplus" class="button"><img src="../images/gplus.png"></div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://github.com/btholt" target="_blank"><div id="github" class="button"><img src="../images/github.png"></div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://bitbucket.org/btholt" target="_blank"><div id="bitbucket" class="button"><img src="../images/bitbucket-white.png"></div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://lanyrd.com/btholt" target="_blank"><div id="lanyrd" class="button"><img src="../images/lanyrd.png"></div></a>
			</div>
			<div class="col col-1-3">
				<a href="http://linkedin.com/in/btholt" target="_blank"><div id="linkedin" class="button"><img src="../images/linkedin.png"></div></a>
			</div>
		</div>
	</section>
	<?php include_once("analytics.php"); ?>
</body>
</html>