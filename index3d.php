<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>brianholt.me | 3d index</title>
	<link rel="stylesheet" href="css/home-3d.css">
	<link rel="icon" type="image/png" href="favicon.png">
	</head>
	<body>

		<section class="siteHead">
			<h1>brian holt</h1>
			<span class="siteHeadText">web developer, innovator, husband, badass</span>
		</section>

		<section id="threejs-body">
		</section>

		<section class="buttonWrap">
			<div class="buttonBody">
				<div class="col col-1-3">
					<a href="http://blog.brianholt.me"><div id="blog" class="button">blog</div></a>
				</div>
				<div class="col col-1-3">
					<a href="portfolio.php"><div id="portfolio" class="button">portfolio</div></a>
				</div>
				<div class="col col-1-3">
					<a href="workwithme.php"><div id="work" class="button">work with me</div></a>
				</div>
			</div>
		</section>

		<script src="components/threejs/build/three.min.js"></script>
		<script src="components/threejs/examples/js/Detector.js"></script>
		<script src="js/3d-experiment.js"></script>
		<?php include_once("analytics.php"); ?>
	</body>
	
</body>
</html>