//////////	
// MAIN //
//////////

// standard global variables
var container, scene, camera, renderer;

// custom global variables
var cube, shape, spotlight3, lightTarget;

var spotLight;

// initialization
init();

// animation loop / game loop
animate();

///////////////
// FUNCTIONS //
///////////////
			
function init() 
{
	///////////
	// SCENE //
	///////////
	scene = new THREE.Scene();

	////////////
	// CAMERA //
	////////////
	
	// set the view size in pixels (custom or according to window size)
	// var SCREEN_WIDTH = 400, SCREEN_HEIGHT = 300;
	var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;	
	// camera attributes
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
	// set up camera
	camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
	// add the camera to the scene
	scene.add(camera);
	// the camera defaults to position (0,0,0)
	// 	so pull it back (z = 400) and up (y = 100) and set the angle towards the scene origin
	camera.position.set(0,0,400);
	camera.lookAt(scene.position);	

	//////////////
	// RENDERER //
	//////////////
	
	// create and start the renderer; choose antialias setting.
	if ( Detector.webgl )
		renderer = new THREE.WebGLRenderer( {antialias:true} );
	else
		renderer = new THREE.CanvasRenderer(); 
	
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	// create a div element to contain the renderer
	container = document.createElement( 'div' );
	document.body.appendChild( container );
	// alternatively: if you insert the div via HTML, access it using
	//   container = document.getElementById( 'container' );

	// attach renderer to the container div
	container.appendChild( renderer.domElement );
	
	////////////
	// EVENTS //
	////////////

	document.addEventListener( 'mousemove', onMouseMove, false );
	window.addEventListener( 'resize', onWindowResize, false );
	
	///////////
	// LIGHT //
	///////////
	
	var ambientLight = new THREE.AmbientLight(0x111111);
	scene.add(ambientLight);

	scene.add( spotLight );
	
	//////////////
	// GEOMETRY //
	//////////////
		

	spotlight3 = new THREE.SpotLight(0x0000ff);
	spotlight3.position.set(150,80,50);
	spotlight3.shadowCameraVisible = true;
	spotlight3.shadowDarkness = 0.95;
	spotlight3.intensity = 20;
	spotlight3.castShadow = true;
	scene.add(spotlight3);
	// change the direction this spotlight is facing
	lightTarget = new THREE.Object3D();
	lightTarget.position.set(150,80,0);
	scene.add(lightTarget);
	spotlight3.target = lightTarget;

	// Create an array of materials to be used in a cube, one for each side
	var cubeMaterialArray = [];
	// order to add materials: x+,x-,y+,y-,z+,z-
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0xff3333 } ) );
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0xff8800 } ) );
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0xffff33 } ) );
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0x33ff33 } ) );
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0x3333ff } ) );
	cubeMaterialArray.push( new THREE.MeshLambertMaterial( { color: 0x8833ff } ) );
	var cubeMaterials = new THREE.MeshFaceMaterial( cubeMaterialArray );
	// Cube parameters: width (x), height (y), depth (z), 
	//        (optional) segments along x, segments along y, segments along z
	var cubeGeometry = new THREE.CubeGeometry( 20, 20, 20, 1, 1, 1 );
	// using THREE.MeshFaceMaterial() in the constructor below
	//   causes the mesh to use the materials stored in the geometry
	cube = new THREE.Mesh( cubeGeometry, cubeMaterials );
	cube.position.set(-100, 100, 5);
	scene.add( cube );		

	var sphereGeometry = new THREE.SphereGeometry( 10, 16, 8 );
	var darkMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
	var wireframeMaterial = new THREE.MeshBasicMaterial( 
		{ color: 0xffffff, wireframe: true, transparent: true } ); 
	shape = THREE.SceneUtils.createMultiMaterialObject( 
		sphereGeometry, [ darkMaterial, wireframeMaterial ] );
	shape.position = spotlight3.position;
	scene.add( shape );

	// create a set of coordinate axes to help orient user
	//    specify length in pixels in each direction
	var axes = new THREE.AxisHelper(1000);
	scene.add( axes );
	
	///////////
	// FLOOR //
	///////////
	
	// note: 4x4 checkboard pattern scaled so that each square is 25 by 25 pixels.
	var floorTexture = new THREE.ImageUtils.loadTexture( '../images/checkerboard.jpg' );
	floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping; 
	floorTexture.repeat.set( 10, 10 );
	// DoubleSide: render texture on bloth sides of mesh
	var floorMaterial = new THREE.MeshLambertMaterial( { map: floorTexture, side: THREE.DoubleSide } );
	var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 1, 1);
	var floor = new THREE.Mesh(floorGeometry, floorMaterial);
	// floor.position.y = -0.5;
	// floor.rotation.x = Math.PI / 2;
	scene.add(floor);
	
	// fog must be added to scene before first render
	scene.fog = new THREE.FogExp2( 0x999900, 0.00025 );
}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

function onMouseMove(event) {
	var adjustedX = (event.clientX - window.innerWidth / 2)/2.5;
	var adjustedY = -((event.clientY - window.innerHeight / 2))/2.5;
	// cube.position.set(adjustedX, adjustedY, 10);	
	// spotLight.position.set(adjustedX, adjustedY, 10);	
	// spotLight.target.position.set(adjustedX, adjustedY, 0);	
	spotlight3.position.set(adjustedX, adjustedY, 200);
	shape.position = spotlight3.position;
	spotlight3.target.position.set(adjustedX,adjustedY,0);
	cube.position = spotlight3.target.position;
	console.log("client[" + event.clientX + "," + event.clientY + "] window[" + window.innerWidth + "," + window.innerHeight + "]");
	console.log("adjusted[" + adjustedX + "," + adjustedY + "]");
}

function animate() 
{
    requestAnimationFrame( animate );
	render();
}

function render() 
{	
	renderer.render( scene, camera );
}