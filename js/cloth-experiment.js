var pinsFormation = [];
var pins = [6];

pinsFormation.push( pins );

pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
pinsFormation.push( pins );

pins = [ 0 ];
pinsFormation.push( pins );

pins = []; // cut the rope ;)
pinsFormation.push( pins );

pins = [ 0, cloth.w ]; // classic 2 pins
pinsFormation.push( pins );

pins = pinsFormation[ 1 ];


function togglePins() {

	pins = pinsFormation[ ~~( Math.random() * pinsFormation.length ) ];

}

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

var container, stats;
var camera, scene, renderer, projector;

var clothGeometry;
var sphere;
var object, arrow;
var planes = [];
var rotate = false;
init();
animate();

function init() {
	container = document.createElement( 'div' );
	document.body.appendChild( container );

	// scene

	scene = new THREE.Scene();

	scene.fog = new THREE.Fog( 0xcce0ff, 500, 10000 );
	projector = new THREE.Projector();

	// camera

	camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.y = 100;
	camera.position.z = 800;
	scene.add( camera );

	// lights

	var light, materials;

	scene.add( new THREE.AmbientLight( 0x666666 ) );

	light = new THREE.DirectionalLight( 0xdfebff, 1.75 );
	light.position.set( 50, 200, 100 );
	light.position.multiplyScalar( 1.3 );

	light.castShadow = true;
	//light.shadowCameraVisible = true;

	light.shadowMapWidth = 2048;
	light.shadowMapHeight = 2048;

	var d = 300;

	light.shadowCameraLeft = -d;
	light.shadowCameraRight = d;
	light.shadowCameraTop = d;
	light.shadowCameraBottom = -d;

	light.shadowCameraFar = 1000;
	light.shadowDarkness = 0.5;

	scene.add( light );

	light = new THREE.DirectionalLight( 0x3dff0c, 0.35 );
	light.position.set( 0, -1, 0 );

	scene.add( light );

	// load web page
	
	// cloth material
	var clothTexture = THREE.ImageUtils.loadTexture( '../images/brianholtme-invert.png' );
	clothTexture.wrapS = clothTexture.wrapT = THREE.RepeatWrapping;
	clothTexture.anisotropy = 16;

	var clothMaterial = new THREE.MeshPhongMaterial( { alphaTest: 0.5, ambient: 0xffffff, color: 0xffffff, specular: 0x030303, emissive: 0x111111, shiness: 10, map: clothTexture, side: THREE.DoubleSide } );

	// cloth geometry
	clothGeometry = new THREE.ParametricGeometry( clothFunction, cloth.w, cloth.h, true );
	clothGeometry.dynamic = true;
	clothGeometry.computeFaceNormals();

	var uniforms = { texture:  { type: "t", value: clothTexture } };
	var vertexShader = document.getElementById( 'vertexShaderDepth' ).textContent;
	var fragmentShader = document.getElementById( 'fragmentShaderDepth' ).textContent;

	// cloth mesh

	object = new THREE.Mesh( clothGeometry, clothMaterial );
	object.position.set( 0, 0, 0 );
	object.castShadow = true;
	object.receiveShadow = true;
	scene.add( object );

	object.customDepthMaterial = new THREE.ShaderMaterial( { uniforms: uniforms, vertexShader: vertexShader, fragmentShader: fragmentShader } );

	// sphere

	var ballGeo = new THREE.SphereGeometry( ballSize, 20, 20 );
	var ballMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff } );

	sphere = new THREE.Mesh( ballGeo, ballMaterial );
	sphere.castShadow = true;
	sphere.receiveShadow = true;
	scene.add( sphere );

	// arrow

	arrow = new THREE.ArrowHelper( new THREE.Vector3( 0, 1, 0 ), new THREE.Vector3( 0, 0, 0 ), 50, 0xff0000 );
	arrow.position.set( -200, 0, -200 );
	// scene.add( arrow );

	// ground

	var initColor = new THREE.Color( 0x497f13 );
	var initTexture = THREE.ImageUtils.generateDataTexture( 1, 1, initColor );

	var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: initTexture } );

	var groundTexture = THREE.ImageUtils.loadTexture( "../components/threejs/examples/textures/terrain/grasslight-big.jpg", undefined, function() { groundMaterial.map = groundTexture } );
	// var groundTexture = THREE.ImageUtils.loadTexture( "../images/water.jpg", undefined, function() { groundMaterial.map = groundTexture } );
	groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
	groundTexture.repeat.set( 25, 25 );
	groundTexture.anisotropy = 16;

	var mesh = new THREE.Mesh( new THREE.PlaneGeometry( 20000, 20000 ), groundMaterial );
	mesh.position.y = -450;
	mesh.rotation.x = - Math.PI / 2;
	mesh.receiveShadow = true;
	scene.add( mesh );

	var toprow = 30;
	var middlerow = -30;
	var bottomrow = -90
	var leftcol = -70;
	var middlecol = -10;
	var rightcol = 45;

	// wall
	var wallGeometry = new THREE.PlaneGeometry(500, 500);
	var wallTexture = THREE.ImageUtils.loadTexture( '../images/wallpaper.jpg' );
	var wallMaterial = new THREE.MeshPhongMaterial( { map : wallTexture } );
	// var wallMaterial  = new THREE.MeshBasicMaterial({color: 0xeeeeee});
	var wall = new THREE.Mesh( wallGeometry, wallMaterial );
	wall.position.x = -375;
	scene.add(wall);
	var wall = new THREE.Mesh( wallGeometry, wallMaterial );
	wall.position.x = 375;
	scene.add (wall);
	var wall = new THREE.Mesh( wallGeometry, wallMaterial );
	wall.position.y = 375;
	scene.add (wall);
	var wall = new THREE.Mesh( wallGeometry, wallMaterial );
	wall.position.y = -425;
	scene.add (wall);

	// DoubleSide: render texture on bloth sides of mesh
	var linkMaterial = new THREE.MeshBasicMaterial( { map: testTexture, side: THREE.DoubleSide, visible: false } );
	var linkGeometry = new THREE.PlaneGeometry(50, 50, 1, 1);

	// links
	var planePositions = [
		{x : leftcol, y : toprow, name : "http://blog.brianholt.me"}, // top left
		{x : middlecol, y : toprow, name : "http://brianholt.me/portfolio.php"}, // top middle
		{x: rightcol, y : toprow, name : "http://brianholt.me/workwithme.php"}, // top right
		{x : leftcol, y : middlerow, name : "http://twitter.com/holtbt"}, // middle left
		{x : middlecol, y : middlerow, name : "http://gplus.to/btholt"}, // middle middle
		{x : rightcol, y : middlerow, name : "http://github.com/btholt"}, // middle right
		{x : leftcol, y : bottomrow, name : "http://bitbucket.org/btholt"}, // bottom left
		{x : middlecol, y: bottomrow, name : "http://lanyrd.com/btholt"}, // bottom middle
		{x : rightcol, y: bottomrow, name : "http://linkedin.com/in/btholt"}, // bottom right
	];
	var testTexture = new THREE.ImageUtils.loadTexture( '../images/checkerboard.jpg' );
	testTexture.wrapS = testTexture.wrapT = THREE.RepeatWrapping; 
	testTexture.repeat.set( 3, 3 );
	// DoubleSide: render texture on bloth sides of mesh
	var linkMaterial = new THREE.MeshBasicMaterial( { map: testTexture, side: THREE.DoubleSide, visible: false } );
	var linkGeometry = new THREE.PlaneGeometry(50, 50, 1, 1);
	for (var i = 0; i < planePositions.length; i++) {
		var pos = planePositions[i];
		var link = new THREE.Mesh(linkGeometry, linkMaterial);
		link.position.x = pos.x;
		link.position.y = pos.y;
		link.name = pos.name;
		scene.add(link);
		planes.push(link);
	}

	// poles

	var poleTexture = THREE.ImageUtils.loadTexture( '../images/wood.jpg' );
	poleTexture.wrapS = poleTexture.wrapT = THREE.RepeatWrapping;
	poleTexture.anisotropy = 16;
	var poleGeo = new THREE.CubeGeometry( 5, 310, 5 );
	// var poleMat = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, shiness: 100 } );
	var poleMat = new THREE.MeshPhongMaterial( { map : poleTexture } );

	var mesh = new THREE.Mesh( poleGeo, poleMat );
	mesh.position.x = -125;
	mesh.position.y = -28;
	mesh.receiveShadow = true;
	mesh.castShadow = true;
	scene.add( mesh );

	var mesh = new THREE.Mesh( poleGeo, poleMat );
	mesh.position.x = 125;
	mesh.position.y = -28;
	mesh.receiveShadow = true;
	mesh.castShadow = true;
	scene.add( mesh );

	var mesh = new THREE.Mesh( new THREE.CubeGeometry( 255, 5, 5 ), poleMat );
	mesh.position.y = -250 + 750/2;
	mesh.position.x = 0;
	mesh.receiveShadow = true;
	mesh.castShadow = true;
	scene.add( mesh );

	// var gg = new THREE.CubeGeometry( 10, 10, 10 );
	// var mesh = new THREE.Mesh( gg, poleMat );
	// mesh.position.y = -250;
	// mesh.position.x = 125;
	// mesh.receiveShadow = true;
	// mesh.castShadow = true;
	// scene.add( mesh );

	// var mesh = new THREE.Mesh( gg, poleMat );
	// mesh.position.y = -250;
	// mesh.position.x = -125;
	// mesh.receiveShadow = true;
	// mesh.castShadow = true;
	// scene.add( mesh );

	var dirtTexture = THREE.ImageUtils.loadTexture( '../images/dirt.jpg' );
	var dirtMat = new THREE.MeshPhongMaterial( { map : dirtTexture } );
	var cubeMaterialArray = [];
    // order to add materials: x+,x-,y+,y-,z+,z-
    cubeMaterialArray.push(poleMat);
    cubeMaterialArray.push(poleMat);
    cubeMaterialArray.push(dirtMat);
    cubeMaterialArray.push(poleMat);
    cubeMaterialArray.push(poleMat);
    cubeMaterialArray.push(poleMat);
    var cubeMaterials = new THREE.MeshFaceMaterial( cubeMaterialArray );
    var cubeGeometry = new THREE.CubeGeometry( 250, 15, 30, 3, 3, 3 );
    cube = new THREE.Mesh( cubeGeometry, cubeMaterials );
    cube.position.set(0, -175, 0);
    cube.receiveShadow = true;
	cube.castShadow = true;
    scene.add( cube );

	// var planterGeo = new THREE.CubeGeometry( 250, 15, 30 );
	// var mesh = new THREE.Mesh(planterGeo, cubeMaterialArray);
	// mesh.position.y = -175;
	// mesh.position.x = 0;
	// mesh.receiveShadow = true;
	// mesh.castShadow = true;
	// scene.add( mesh );

	// flowers
	var loader = new THREE.OBJMTLLoader();
	loader.addEventListener("load", function (event) {
		var plant = event.content;
		plant.position.y = -175;
		plant.position.x = -100;
		plant.scale.x = plant.scale.y = plant.scale.z = 60;
		scene.add(plant);
		var plant = plant.clone();
		plant.position.x = -50;
		plant.rotation.y = Math.PI / 5;
		scene.add(plant);
		var plant = plant.clone();
		plant.position.x = 0;
		plant.rotation.y = 2 * Math.PI / 5;
		scene.add(plant);
		var plant = plant.clone();
		plant.position.x = 50;
		plant.rotation.y = 3 * Math.PI / 5;
		scene.add(plant);
		var plant = plant.clone();
		plant.position.x = 100;
		plant.rotation.y = 4 * Math.PI / 5;
		scene.add(plant);
	}, false); 
	loader.load ("../obj/primroseEdit.obj", "../obj/primroseEdit.mtl");

	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( scene.fog.color );

	container.appendChild( renderer.domElement );

	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.physicallyBasedShading = true;

	renderer.shadowMapEnabled = true;

	//

	stats = new Stats();
	container.appendChild( stats.domElement );

	sphere.visible = !true;

	window.addEventListener( 'resize', onWindowResize, false );

	// events
	document.addEventListener( 'mousedown', onDocumentMouseDown, false );
}

function onDocumentMouseDown( event ) {

	event.preventDefault();

	var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
	projector.unprojectVector( vector, camera );

	var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

	var intersects = raycaster.intersectObjects( planes );

	if ( intersects.length > 0 ) {
		console.log(intersects[0]);
		open_in_new_tab(intersects[0].object.name);
	}
}

function open_in_new_tab(url ) {
  var win=window.open(url, '_blank');
  win.focus();
}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );

	var time = Date.now();

	windStrength = Math.cos( time / 7000 ) * 20 + 40;
	windStrength = windStrength / 10;
	windForce.set( Math.sin( time / 2000 ), Math.cos( time / 3000 ), Math.sin( time / 1000 ) ).normalize().multiplyScalar( windStrength );
	arrow.setLength( windStrength );
	arrow.setDirection( windForce );

	simulate(time);
	render();
	stats.update();

}

function render() {

	var timer = Date.now() * 0.0002;

	var p = cloth.particles;

	for ( var i = 0, il = p.length; i < il; i ++ ) {

		clothGeometry.vertices[ i ].copy( p[ i ].position );

	}

	clothGeometry.computeFaceNormals();
	clothGeometry.computeVertexNormals();

	clothGeometry.normalsNeedUpdate = true;
	clothGeometry.verticesNeedUpdate = true;

	sphere.position.copy( ballPosition );

	if ( rotate ) {

		camera.position.x = Math.cos( timer ) * 1500;
		camera.position.z = Math.sin( timer ) * 1500;

	}

	camera.lookAt( scene.position );

	renderer.render( scene, camera );

}