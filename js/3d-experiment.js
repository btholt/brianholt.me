// standard global variables
var container, scene, camera, renderer, controls, stats, projector;

// custom global variables
var cube, renderElement;

// initialization
init();

// animation loop 
animate();
            
function init() 
{
    scene = new THREE.Scene();
    var SCREEN_WIDTH = 500, SCREEN_HEIGHT = 500;
    var VIEW_ANGLE = 30, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
    camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    camera.position.set(0,0,400);
    camera.lookAt(scene.position); 
    
    if ( Detector.webgl )
        renderer = new THREE.WebGLRenderer( {antialias:true} );
    else
        renderer = new THREE.CanvasRenderer(); 
    
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    container = document.getElementById('threejs-body');
    renderElement = renderer.domElement;
    container.appendChild( renderElement );
    
    projector = new THREE.Projector();
    document.addEventListener( 'click', onDocumentMouseDown, false );
    
    var light = new THREE.PointLight(0xffffff);
    light.position.set(0,0,250);
    scene.add(light);
    var ambientLight = new THREE.AmbientLight("#444");
    scene.add(ambientLight);
    
    // Create an array of materials to be used in a cube, one for each side
    var cubeMaterialArray = [];
    // order to add materials: x+,x-,y+,y-,z+,z-
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name: "http://bitbucket.org/btholt", map: THREE.ImageUtils.loadTexture( 'images/cube/bitbucket.png' ) }));
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name : "http://gplus.to/btholt", map: THREE.ImageUtils.loadTexture( 'images/cube/gplus2.png' ) }));
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name: "http://github.com/btholt", map: THREE.ImageUtils.loadTexture( 'images/cube/github.png' ) }));
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name: "http://twitter.com/holtbt", map: THREE.ImageUtils.loadTexture( 'images/cube/twitter.png' ) }));
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name: "http://linkedin.com/in/btholt", map: THREE.ImageUtils.loadTexture( 'images/cube/linkedin.png' ) }));
    cubeMaterialArray.push(new THREE.MeshLambertMaterial( {name: "http://lanyrd.com/btholt", map: THREE.ImageUtils.loadTexture( 'images/cube/lanyrd.png' ) }));
    var cubeMaterials = new THREE.MeshFaceMaterial( cubeMaterialArray );

    var cubeGeometry = new THREE.CubeGeometry( 100, 100, 100, 1, 1, 1 );
    cube = new THREE.Mesh( cubeGeometry, cubeMaterials );
    cube.position.set(0, 0, 0);
    scene.add( cube );      
    scene.fog = new THREE.FogExp2( 0x999900, 0.00025 );

}

function onDocumentMouseDown( event ) {

    // here you have to account for the fact the camera is not necessarily in the same position that it thinks it is in terms of the entire page as opposed to just the rendering canvas
    var vector = new THREE.Vector3( ( (event.clientX - renderElement.offsetLeft) / renderElement.offsetWidth ) * 2 - 1, - ( (event.clientY - renderElement.offsetTop + document.body.scrollTop) / renderElement.offsetHeight ) * 2 + 1, 0.5 );
    projector.unprojectVector( vector, camera );

    var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    var intersects = raycaster.intersectObjects( [cube] );

    console.log(intersects);

    if ( intersects.length > 0 ) {
        // pull out the stored URL
        open_in_new_tab(intersects[0].object.material.materials[intersects[0].faceIndex].name);
    }
}

function open_in_new_tab(url )
{
  var win=window.open(url, '_blank');
  win.focus();
}

function animate() 
{
    requestAnimationFrame( animate );
    cube.rotation.x += 0.012;
    cube.rotation.y += 0.008;
    render();       
}

function render() 
{   
    renderer.render( scene, camera );
}
