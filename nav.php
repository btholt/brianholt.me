	<div id="mainNavSpace"></div>
	<div id="expandedSpace"></div>
	<nav id="mainNav">
		<div class="smaller">
			<a href="index.php"><div id="logo">brian holt</div></a>
			<div id="tagline">web developer, innovator, husband, badass</div>
			<a href="#showMore"><div id="showMore">show more</div></a>
		</div>
		<div id="expanded">
			<div class="col-nav col-1-9">
				<a href="http://blog.brianholt.me"><div id="blog" class="button smallButton">blog</div>
			</div></a>
			<div class="col-nav col-1-9">
				<a href="portfolio.php"><div id="portfolio" class="button smallButton">portfolio</div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="workwithme.php"><div id="work" class="button smallButton">work with me</div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://twitter.com/holtbt" target="_blank"><div id="twitter" class="button smallButton"><img src="../images/twitter-bird-dark-bgs.png"></div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://gplus.to/btholt" target="_blank"><div id="gplus" class="button smallButton"><img src="../images/gplus.png"></div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://github.com/btholt" target="_blank"><div id="github" class="button smallButton"><img src="../images/github.png"></div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://bitbucket.org/btholt" target="_blank"><div id="bitbucket" class="smallButton button"><img src="../images/bitbucket-white.png"></div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://lanyrd.com/btholt" target="_blank"><div id="lanyrd" class="smallButton button"><img src="../images/lanyrd.png"></div></a>
			</div>
			<div class="col-nav col-1-9">
				<a href="http://linkedin.com/in/btholt" target="_blank"><div id="linkedin" class="smallButton button"><img src="../images/linkedin.png"></div></a>
			</div>
		</div>
	</nav>