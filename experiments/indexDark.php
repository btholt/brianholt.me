<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>brianholt.me | dark room index</title>
	<link rel="icon" type="image/png" href="../favicon.png">
	<style>
		body {
			margin: 0px;
		}
	</style>
	</head>
	<body>

		<!-- <section class="siteHead">
			<h1>brian holt</h1>
			<span class="siteHeadText">web developer, innovator, husband, badass</span>
		</section> -->

		<section id="threejs-body">
		</section>

		<script src="../components/threejs/build/three.min.js"></script>
		<script src="../components/threejs/examples/js/Detector.js"></script>
		<script src="../js/dark-experiment.js"></script>
		<?php include_once("../analytics.php"); ?>
	</body>
	
</body>
</html>