<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>brianholt.me | cloth</title>
	<link rel="stylesheet" href="../css/home-cloth.css">
	<link rel="icon" type="image/png" href="../favicon.png">
</head>
<body>
	<script src="../components/threejs/build/three.min.js"></script>
	<script src="../components/threejs/examples/js/loaders/MTLLoader.js"></script>
		<script src="../components/threejs/examples/js/loaders/OBJMTLLoader.js"></script>
	<script src="../components/threejs/examples/js/Detector.js"></script>
	<script src="../components/threejs/examples/js/libs/stats.min.js"></script>

	<script src="../components/threejs/examples/js/Cloth.js"></script>

	<script type="x-shader/x-fragment" id="fragmentShaderDepth">

		uniform sampler2D texture;
		varying vec2 vUV;

		vec4 pack_depth( const in float depth ) {

			const vec4 bit_shift = vec4( 256.0 * 256.0 * 256.0, 256.0 * 256.0, 256.0, 1.0 );
			const vec4 bit_mask  = vec4( 0.0, 1.0 / 256.0, 1.0 / 256.0, 1.0 / 256.0 );
			vec4 res = fract( depth * bit_shift );
			res -= res.xxyz * bit_mask;
			return res;

		}

		void main() {

			vec4 pixel = texture2D( texture, vUV );

			if ( pixel.a < 0.5 ) discard;

			gl_FragData[ 0 ] = pack_depth( gl_FragCoord.z );

		}
	</script>

	<script type="x-shader/x-vertex" id="vertexShaderDepth">

		varying vec2 vUV;

		void main() {

			vUV = 0.75 * uv;

			vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );

			gl_Position = projectionMatrix * mvPosition;

		}

	</script>
	<script src="../js/cloth-experiment.js"></script>
	<?php include_once("../analytics.php"); ?>
</body>
</html>