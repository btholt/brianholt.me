<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>work with me | brianholt.me</title>
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/workwithme.css">
	<link rel="icon" type="image/png" href="favicon.png">
</head>
<body>
<?php include "nav.php" ?>

<div class="module">
	<h2>work with me</h2>
	<p>
		So you want to work with me? Feel free to tweet at me or send me an email. I'm always looking for great open source work to do and even I am even open to the occasional piece of contract work.
	</p>
	<p>
		Recruiter? Feel free to contact me but make sure to at least glance over my <a href="http://linkedin.com/in/btholt" target="_blank">LinkedIn</a> first. I'm not interested in a job in downtown Chicago doing Android at $35,000 a year with no relocation (a call I actually fielded this week.)
	</p>
	<p>
		Anything else? <a href="http://twitter.com/holtbt" target="_blank">Twitter</a> and <a href="mailto:brian@brianholt.me">email</a> are the best ways to contact me.
	</p>
</div>


	
<script type="text/javascript" src="js/global-ck.js"></script>
<?php include_once("analytics.php"); ?>
</body>
</html>